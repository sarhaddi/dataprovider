<?php

namespace app\models\dataProvider;

use app\models\User;
use app\services\dataProvider\model\DbDataProviderModel;
use Yii;

/**
 * Class UserDataprovider
 * @package app\models
 */
class UserFromDatabase extends DbDataProviderModel
{
    public static function model()
    {
        return User::class;
    }

    public function meta()
    {
        return [
            'title' => 'users'
        ];
    }

    public function attributeTypes()
    {
        return [
            'id' => self::TYPE_INTEGER,
            'first_name' => self::TYPE_STRING,
            'last_name' => self::TYPE_STRING,
            'status' => ['active','pending'],
            'birthdate' => self::TYPE_DATE,
            'follower' => self::TYPE_INTEGER,
        ];
    }

    public function attributeWidths()
    {
        return [
            'id' => 5,
            'first_name' => 20,
            'last_name' => 20,
            'status' => 15,
            'birthdate' => 20,
            'follower' => 20,
        ];
    }

//    public static function getDb()
//    {
//        return Yii::$app->getDb();
//    }

//    public static function tableName()
//    {
//        return 'user';
//    }

//    public static function attributes()
//    {
//        return ['id','first_name','last_name','status','birthdate','follower'];
//    }

//    public function attributeLabels()
//    {
//        return [
//            'id' => 'Id',
//            'first_name' => 'First Name',
//            'last_name' => 'Last Name',
//            'status' => 'Status',
//            'birthdate' => 'Birthdate',
//            'follower' => 'Follower',
//        ];
//    }

//    public static function attributeFilters()
//    {
//        return ['id','first_name','last_name','status','birthdate','follower'];
//    }

//    public function defaultFilter()
//    {
//        return [];
//    }

//    public static function attributesSort()
//    {
//        return ['id','first_name','last_name','status','birthdate','follower'];
//    }

//    public function defaultSort()
//    {
//        return ['id' => SORT_ASC];
//    }

//    public $paginationDefaultPageSize = 20;
//    public $paginationPageSizeLimit = [1, 50];
}
