<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210702_022753_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'status' => "ENUM('pending','active')",
            'birthdate' => $this->date(),
            'follower' => $this->integer(),
        ]);

        $first_names = ['Ali', 'Reza', 'Akbar', 'Azar', 'Arash', 'Hamid', 'Kobra', 'Nima', 'Navid'];
        $last_names = ['Mohammadi', 'Rezaee', 'Abdi', 'Saeedi', 'Kamrani', 'Elahi', 'Tahery', 'Kareshki', 'Akbari'];
        $status = ['active', 'pending'];
        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                $first_names[rand(0, sizeof($first_names) - 1)],
                $last_names[rand(0, sizeof($last_names) - 1)],
                $status[rand(0, sizeof($status) - 1)],
                rand(1940, 2020) . '-' . rand(1, 12) . '-' . rand(1, 30),
                rand(0, 99999)
            ];
        }

        $this->batchInsert('user', ['first_name', 'last_name', 'status', 'birthdate', 'follower'], $data);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
