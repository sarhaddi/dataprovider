<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\services\dataProvider\Attribute;
use app\services\dataProvider\db\DataProvider;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Query;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        var_dump((new DataProvider([
            'source' => (new Query())->select('*')->from('blog_post'),
            'attributes' => [
                'title' => [
                    'type' => Attribute::TYPE_STRING,
                    'width' => 0
                ]
            ]
        ]))->getData());
        echo $message . "\n";

        return ExitCode::OK;
    }
}
