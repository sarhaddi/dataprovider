<?php

namespace app\controllers;

use app\models\dataProvider\UserFromDatabase;
use app\models\dataProvider\UserFromJson;
use app\models\User;
use app\models\UserDataProvider;
use app\services\dataProvider\action\DataProviderDataAction;
use app\services\dataProvider\action\DataProviderMetaAction;
use app\services\dataProvider\Attribute;
use app\services\dataProvider\data\DataProvider;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page', 'X-Pagination-Page-Count', 'X-Pagination-Per-Page', 'X-Pagination-Total-Count'],
            ]
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'data' => [
                'class' => DataProviderDataAction::class,
                'modelClass' => UserFromDatabase::class
            ],
            'meta' => [
                'class' => DataProviderMetaAction::class,
                'modelClass' => UserFromDatabase::class
            ],
            'data2' => [
                'class' => DataProviderDataAction::class,
                'modelClass' => UserFromJson::class
            ],
            'meta2' => [
                'class' => DataProviderMetaAction::class,
                'modelClass' => UserFromJson::class
            ],
        ];
    }
}
