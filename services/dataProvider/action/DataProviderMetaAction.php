<?php
namespace app\services\dataProvider\action;


class DataProviderMetaAction extends \yii\base\Action
{
    public $modelClass;

    public function init()
    {
        if ($this->modelClass === null) {
            throw new \yii\base\InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }
    }

    public function run()
    {
        $model = new $this->modelClass;
        return $model->getDataProvider()->getMeta();
    }
}