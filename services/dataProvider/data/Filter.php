<?php


namespace app\services\dataProvider\data;

use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\base\Filter as BaseFilter;


/**
 * Class Filter
 * @package app\services\dataProvider\data
 */
class Filter extends BaseFilter
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $filters = $this->getFilter($provider->attributes->attributes);
        if (!is_array($filters))
            return;
        $provider->source = array_values(array_filter($provider->source, function ($row) use ($filters) {
            if (is_array($row)) {
                foreach ($filters as $key => $filter) {
                    if (is_array($filter) and sizeof($filter) == 3) {
                        if ($filter[0] == 'like') {
                            if (!isset($row[$filter[1]]) or !preg_match('/^.*' . $filter[2] . '.*$/', $row[$filter[1]]))
                                return false;
                        } elseif ($filter[0] == '=') {
                            if (!isset($row[$filter[1]]) or $filter[2] != $row[$filter[1]])
                                return false;
                        } elseif ($filter[0] == '!=') {
                            if (isset($row[$filter[1]]) and $filter[2] == $row[$filter[1]])
                                return false;
                        }
                    } else {
                        if (!isset($row[$key]) or $row[$key] != $filter)
                            return false;
                    }
                }
            }
            if (is_object($row)) {
                foreach ($filters as $key => $filter) {
                    if (is_array($filter) and sizeof($filter) == 3) {
                        $attr = $filter[1];
                        if ($filter[0] == 'like') {
                            if (!isset($row->$attr) or !preg_match('/^.*' . $filter[2] . '.*$/', $row->$attr))
                                return false;
                        } elseif ($filter[0] == '=') {
                            if (!isset($row->$attr) or $filter[2] != $row->$attr)
                                return false;
                        } elseif ($filter[0] == '!=') {
                            if (isset($row->$attr) and $filter[2] == $row->$attr)
                                return false;
                        }
                    } else {
                        if (!isset($row->$key) or $row->$key != $filter)
                            return false;
                    }
                }
            }
            return true;
        }));
    }
}
