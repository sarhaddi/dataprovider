<?php


namespace app\services\dataProvider\data;

use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\base\Sort as BaseSort;
use yii\helpers\ArrayHelper;


class Sort extends BaseSort
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $orders = $this->getOrders();
        if (!empty($orders)) {
            ArrayHelper::multisort($provider->source, array_keys($orders), array_values($orders), SORT_REGULAR);
        }
    }
}