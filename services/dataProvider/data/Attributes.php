<?php


namespace app\services\dataProvider\data;

use \app\services\dataProvider\base\Attributes as BaseAttributes;
use app\services\dataProvider\base\DataProvider;


/**
 * Class Attributes
 * @package app\services\dataProvider\data
 */
class Attributes extends BaseAttributes
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $keys = array_keys($this->attributes);
        $provider->source = array_map(function ($el) use ($keys) {
            $row = [];
            if (is_array($el))
                foreach ($keys as $key) {
                    $row[$key] = $el[$key] ?? null;
                }
            if (is_object($el))
                foreach ($keys as $key) {
                    $row[$key] = $el->$key ?? null;
                }
            return $row;
        }, $provider->source);
    }
}