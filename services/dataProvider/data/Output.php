<?php


namespace app\services\dataProvider\data;

use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\base\Output as BaseMeta;

/**
 * Class Meta
 * @package app\services\dataProvider\data
 */
class Output extends BaseMeta
{
    /**
     * @param DataProvider $provider
     * @return array
     */
    public function data($provider)
    {
        return $provider->source;
    }
}