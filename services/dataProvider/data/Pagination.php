<?php


namespace app\services\dataProvider\data;

use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\base\Pagination as BasePagination;

/**
 * Class Pagination
 * @package app\services\dataProvider\data
 */
class Pagination extends BasePagination
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        parent::data($provider);
        $provider->source = array_values(array_slice($provider->source, $this->offset, $this->limit));
    }

    public function getTotalCount($provider)
    {
        return sizeof($provider->source);
    }
}