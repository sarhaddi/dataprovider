<?php

namespace app\services\dataProvider\model;

use app\services\dataProvider\data\Attributes;
use app\services\dataProvider\data\Filter;
use app\services\dataProvider\data\Output;
use app\services\dataProvider\data\Pagination;
use app\services\dataProvider\data\Sort;
use yii\base\InvalidConfigException;

class ArrayDataProviderModel extends BaseDataProviderModel
{
    /**
     * @return array return source data
     */
    public function source()
    {
        return [];
    }

    /**
     * @return string[] the attributes list
     * @throws InvalidConfigException
     */
    public static function attributes()
    {
        return [];
    }

    /**
     * @return Attributes|Object get attribute object
     * @throws InvalidConfigException
     */
    public function getAttributes()
    {
        return \Yii::createObject([
            'class' => Attributes::class,
            'attributes' => $this->attributeOptions()
        ]);
    }

    /**
     * @return Filter|object the attribute filters
     * @throws InvalidConfigException
     * @see attributeFilters()
     */
    public function getFilters()
    {
        return \Yii::createObject([
            'class' => Filter::class,
            'attributes' => array_values(array_intersect(static::attributes(), static::attributeFilters())),
            'defaultFilters' => $this->defaultFilter()
        ]);
    }

    /**
     * @return object|Sort
     * @throws InvalidConfigException
     * @see attributeFilters()
     */
    public function getSorts()
    {
        return \Yii::createObject([
            'class' => Sort::class,
            'attributes' => array_values(array_intersect(static::attributes(), static::attributeSorts())),
            'defaultOrder' => $this->defaultSort()
        ]);
    }

    public function getPagination()
    {
        return \Yii::createObject([
            'class' => Pagination::class,
            'defaultPageSize' => $this->paginationDefaultPageSize,
            'pageSizeLimit' => $this->paginationPageSizeLimit
        ]);
    }

    public function getOutput()
    {
        return \Yii::createObject([
            'class' => Output::class,
            'meta' => $this->meta()
        ]);
    }
}