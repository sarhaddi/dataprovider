<?php

namespace app\services\dataProvider\model;

use app\services\dataProvider\db\Attributes;
use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\db\Filter;
use app\services\dataProvider\db\Output;
use app\services\dataProvider\db\Pagination;
use app\services\dataProvider\db\Sort;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\db\TableSchema;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class DbDataProviderModel extends BaseDataProviderModel
{
    /**
     * @return ActiveRecord|false return Model class or false if model not exist
     */
    public static function model()
    {
        return false;
    }

    private static $_model = null;

    /**
     * @return ActiveRecord|false|null Object
     * @throws InvalidConfigException
     */
    private static function getModel()
    {
        if (static::$_model === null) {
            if ($class = static::model()) {
                static::$_model = new $class;
                if (!(static::$_model instanceof ActiveRecord))
                    throw new InvalidConfigException('The model class does not instance of ActiveRecord: ' . static::model());
            } else {
                static::$_model = false;
            }
        }

        return static::$_model;
    }

    /**
     * @return Connection
     * @throws InvalidConfigException
     */
    public static function getDb()
    {
        if (static::getModel()) {
            return static::getModel()->getDb();
        }
        return \Yii::$app->getDb();
    }

    /**
     * @return string table name
     * @throws InvalidConfigException
     */
    public static function tableName()
    {
        if (static::getModel()) {
            return static::getModel()->tableName();
        }
        return '{{%' . Inflector::camel2id(StringHelper::basename(get_called_class()), '_') . '}}';
    }

    /**
     * @return string[] the attributes list
     * @throws InvalidConfigException
     */
    public static function attributes()
    {
        if (static::getModel()) {
            return static::getModel()->attributes();
        }
        return array_keys(static::getTableSchema()->columns);
    }

    /**
     * Returns the schema information of the DB table associated with this class.
     * @return TableSchema the schema information of the DB table associated with this class.
     * @throws InvalidConfigException if the table for the class does not exist.
     * @throws NotSupportedException
     */
    private static function getTableSchema()
    {
        $tableSchema = static::getDb()
            ->getSchema()
            ->getTableSchema(static::tableName());

        if ($tableSchema === null) {
            throw new InvalidConfigException('The table does not exist: ' . static::tableName());
        }

        return $tableSchema;
    }

    /**
     * @return Attributes|Object get attribute object
     * @throws InvalidConfigException
     */
    public function getAttributes()
    {
        return \Yii::createObject([
            'class' => Attributes::class,
            'attributes' => $this->attributeOptions()
        ]);
    }

    /**
     * @return Filter|object the attribute filters
     * @throws InvalidConfigException
     * @see attributeFilters()
     */
    public function getFilters()
    {
        return \Yii::createObject([
            'class' => Filter::class,
            'attributes' => array_values(array_intersect(static::attributes(), static::attributeFilters())),
            'defaultFilters' => $this->defaultFilter()
        ]);
    }

    /**
     * @return object|Sort
     * @throws InvalidConfigException
     * @see attributeFilters()
     */
    public function getSorts()
    {
        return \Yii::createObject([
            'class' => Sort::class,
            'attributes' => array_values(array_intersect(static::attributes(), static::attributeSorts())),
            'defaultOrder' => $this->defaultSort()
        ]);
    }

    /**
     * @return Query
     * @throws InvalidConfigException
     */
    public function source()
    {
        return (new Query())->select(self::attributes())->from(static::tableName());
    }

    public function getPagination()
    {
        return \Yii::createObject([
            'class' => Pagination::class,
            'defaultPageSize' => $this->paginationDefaultPageSize,
            'pageSizeLimit' => $this->paginationPageSizeLimit
        ]);
    }

    public function getOutput()
    {
        return \Yii::createObject([
            'class' => Output::class,
            'meta' => $this->meta()
        ]);
    }

    /**
     * @throws InvalidConfigException
     */
    public function getDataProvider()
    {
        return \Yii::createObject([
            'class' => DataProvider::class,
            'source' => $this->source(),
            'attributes' => $this->getAttributes(),
            'filter' => $this->getFilters(),
            'sort' => $this->getSorts(),
            'pagination' => $this->getPagination(),
            'output' => $this->getOutput(),
        ]);
    }
}