<?php

namespace app\services\dataProvider\model;

use app\services\dataProvider\base\Attribute;
use app\services\dataProvider\base\Attributes;
use app\services\dataProvider\base\DataProvider;
use app\services\dataProvider\base\Filter;
use app\services\dataProvider\base\Output;
use app\services\dataProvider\base\Pagination;
use app\services\dataProvider\base\Sort;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\helpers\Inflector;

class BaseDataProviderModel extends BaseObject
{
    const TYPE_STRING = Attribute::TYPE_STRING;
    const TYPE_INTEGER = Attribute::TYPE_INTEGER;
    const TYPE_FLOAT = Attribute::TYPE_FLOAT;
    const TYPE_DATE = Attribute::TYPE_DATE;
    const TYPE_ENUM = Attribute::TYPE_ENUM;

    /**
     * @return string[] the attributes list
     * @throws InvalidConfigException
     */
    public static function attributes()
    {
        return [];
    }

    /**
     * @return string[] the attribute labels
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see generateAttributeLabel()
     * @see attributeLabels()
     */
    public function getAttributeLabel($attribute)
    {
        $labels = $this->attributeLabels();
        if (isset($labels[$attribute])) {
            return $labels[$attribute];
        }

        return Inflector::camel2words($attribute, true);
    }

    /**
     * @return string[] the attribute types
     */
    public function attributeTypes()
    {
        return [];
    }

    /**
     * Returns the type for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the type
     * @see attributeTypes()
     */
    public function getAttributeType($attribute)
    {
        $types = $this->attributeTypes();
        if (isset($types[$attribute])) {
            if (is_array($types[$attribute])) {
                return static::TYPE_ENUM;
            }
            return $types[$attribute];
        }

        return static::TYPE_STRING;
    }


    /**
     * Returns the option for the specified enum attribute.
     * @param string $attribute the attribute name
     * @return array the options
     * @see attributeTypes()
     */
    public function getAttributeOptions($attribute)
    {
        $types = $this->attributeTypes();
        if (isset($types[$attribute]) and is_array($types[$attribute])) {
            return $types[$attribute];
        }

        return [];
    }

    /**
     * @return int[] the attribute widths
     */
    public function attributeWidths()
    {
        return [];
    }

    /**
     * Returns the width for the specified attribute.
     * @param string $attribute the attribute name
     * @return integer the width
     * @see attributeWidths()
     */
    public function getAttributeWidth($attribute)
    {
        $widths = $this->attributeWidths();
        if (isset($widths[$attribute])) {
            return $widths[$attribute];
        }

        return 0;
    }

    /**
     * @return Attribute[]
     * @throws InvalidConfigException
     */
    public function attributeOptions()
    {
        $attributes = [];
        foreach (static::attributes() as $attribute) {
            $attributes[$attribute] = \Yii::createObject([
                'class' => Attribute::class,
                'type' => $this->getAttributeType($attribute),
                'width' => $this->getAttributeWidth($attribute),
                'label' => $this->getAttributeLabel($attribute),
                'options' => $this->getAttributeOptions($attribute)
            ]);
        }
        return $attributes;
    }

    /**
     * @return string[] the attributes user can filtered
     * @throws InvalidConfigException
     */
    public static function attributeFilters()
    {
        return static::attributes();
    }

    /**
     * @return array default query condition
     * @see Query::where()
     */
    public function defaultFilter()
    {
        return [];
    }

    /**
     * @return string[] the attributes user can sorted
     * @throws InvalidConfigException
     */
    public static function attributeSorts()
    {
        return static::attributes();
    }

    /**
     * @return array default orderBy in query
     * @see Query::orderBy()
     */
    public function defaultSort()
    {
        return [];
    }

    public $paginationDefaultPageSize = 20;
    public $paginationPageSizeLimit = [1, 50];

    /**
     * @return array more data for font
     */
    public function meta()
    {
        return [];
    }
    /**
     * @return array return source data
     */
    public function source()
    {
        return [];
    }

    /**
     * @return Attributes|Object get attribute object
     */
    public function getAttributes()
    {
        return new Attributes();
    }

    /**
     * @return Filter|object the attribute filters
     * @see attributeFilters()
     */
    public function getFilters()
    {
        return new Filter();
    }

    /**
     * @return object|Sort
     * @see attributeFilters()
     */
    public function getSorts()
    {
        return new Sort();
    }

    public function getPagination()
    {
        return new Pagination();
    }

    public function getOutput()
    {
        return new Output();
    }

    /**
     * @throws InvalidConfigException
     */
    public function getDataProvider()
    {
        return \Yii::createObject([
            'class' => DataProvider::class,
            'source' => $this->source(),
            'attributes' => $this->getAttributes(),
            'filter' => $this->getFilters(),
            'sort' => $this->getSorts(),
            'pagination' => $this->getPagination(),
            'output' => $this->getOutput(),
        ]);
    }
}