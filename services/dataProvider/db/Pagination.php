<?php


namespace app\services\dataProvider\db;

use app\services\dataProvider\base\Pagination as BasePagination;

/**
 * Class Pagination
 * @package app\services\dataProvider\db
 */
class Pagination extends BasePagination
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        parent::data($provider);
        $provider->source->offset($this->offset);
        $provider->source->limit($this->limit);
    }

    public function getTotalCount($provider)
    {
        return (int) $provider->source->count();
    }
}