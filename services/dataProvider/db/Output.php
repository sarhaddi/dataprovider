<?php


namespace app\services\dataProvider\db;

use app\services\dataProvider\base\Output as BaseOutput;

/**
 * Class Meta
 * @package app\services\dataProvider\db
 */
class Output extends BaseOutput
{
    /**
     * @param DataProvider $provider
     * @return array
     */
    public function data($provider)
    {
        return $provider->source->all();
    }
}