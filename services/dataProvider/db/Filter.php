<?php


namespace app\services\dataProvider\db;

use app\services\dataProvider\base\Filter as BaseFilter;

/**
 * Class Filter
 * @package app\services\dataProvider\db
 */
class Filter extends BaseFilter
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $filter = $this->getFilter($provider->attributes->attributes);
        $provider->source->andWhere(array_merge(['and'], $filter));
    }
}