<?php


namespace app\services\dataProvider\db;

use app\services\dataProvider\base\Attributes as BaseAttributes;


/**
 * Class Attributes
 * @package app\services\dataProvider\db
 */
class Attributes extends BaseAttributes
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $provider->source->select($this->getColumns());
    }
}