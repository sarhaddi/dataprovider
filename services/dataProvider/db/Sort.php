<?php


namespace app\services\dataProvider\db;

use app\services\dataProvider\base\Sort as BaseSort;

/**
 * Class Sort
 * @package app\services\dataProvider\db
 */
class Sort extends BaseSort
{
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $orders = $this->getOrders();
        if (!empty($orders))
            $provider->source->orderBy($this->getOrders());
    }
}