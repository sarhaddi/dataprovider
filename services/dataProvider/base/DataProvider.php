<?php


namespace app\services\dataProvider\base;


use yii\base\InvalidArgumentException;

/**
 * Class DataProvider
 * @property Attributes | bool $attributes
 * @property Filter | bool $filter
 * @property Sort | bool $sort
 * @property Pagination | bool $pagination
 * @property Output $output
 * @package app\services\dataProvider\data
 */
class DataProvider extends \yii\base\Component
{
    public $source;
    public $attributes;
    public $filter;
    public $sort;
    public $pagination;
    public $output;

    /**
     * @return array
     */
    public function getData()
    {
        $this->filter->data($this);
        $this->sort->data($this);
        $this->pagination->data($this);
        $this->attributes->data($this);
        return $this->output->data($this);
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        $this->filter->meta($this);
        $this->sort->meta($this);
        $this->pagination->meta($this);
        $this->attributes->meta($this);
        return $this->output->meta;
    }
}