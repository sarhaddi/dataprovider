<?php

namespace app\services\dataProvider\base;


use yii\base\BaseObject;


/**
 * Class Attribute
 * @property string $type
 * @property integer | bool $width
 * @property string $label
 * @property array $options
 * @package app\services\dataProvider\db
 */
class Attribute extends BaseObject
{
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';
    const TYPE_DATE = 'date';
    const TYPE_ENUM = 'enum';
    public $type;
    public $width;
    public $label;
    public $options;

    public function toArray()
    {
        return [
            'type' => $this->type,
            'width' => $this->width,
            'label' => $this->label,
            'options' => $this->options
        ];
    }
}
