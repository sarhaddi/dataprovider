<?php

namespace app\services\dataProvider\base;



/**
 * Class Attributes
 * @property Attribute[] $config
 * @package app\services\dataProvider\base
 */
class Attributes extends Unit
{
    public $attributes = [];

    public function getColumns()
    {
        $columns = [];
        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();
        if (isset($params['attributes'])) {
            foreach ($this->parseAttributesParam($params['attributes']) as $attribute) {
                if (isset($this->attributes[$attribute])) {
                    $columns[] = $attribute;
                }
            }
        }
        return $columns;
    }

    protected function parseAttributesParam($param)
    {
        return is_scalar($param) ? explode(',', $param) : [];
    }

    /**
     * @param DataProvider $provider
     */
    public function meta($provider)
    {
        $config = [];
        foreach ($this->attributes as $index => $attribute) {
            $config[$index] = $attribute->toArray();
        }
        $provider->output->addMeta('attributes', $config);
    }
}