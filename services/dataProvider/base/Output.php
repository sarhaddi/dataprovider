<?php

namespace app\services\dataProvider\base;

use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class Meta
 * @property array $meta
 * @package app\services\dataProvider\base
 */
class Output extends BaseObject
{
    protected $_meta = [];

    public function getMeta()
    {
        if ($this->_meta === null) {
            $this->setData([]);
        }
        return $this->_meta;
    }

    public function setMeta($value)
    {
        if (is_array($value)) {
            $this->_meta = $value;
        } else {
            throw new InvalidArgumentException('Only array is allowed.');
        }
    }

    public function addMeta($name, $value)
    {
        $this->_meta[$name] = $value;
    }

    /**
     * @param DataProvider $provider
     * @return array
     */
    public function data($provider)
    {
        return [];
    }
}