<?php

namespace app\services\dataProvider\base;



class Filter extends Unit
{
    public $attributes;
    public $defaultFilters;

    public function getFilter($attributes)
    {
        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $filters = [];
        foreach (array_intersect($this->attributes, array_keys($params)) as $name) {
            if (!isset($attributes[$name]) or $attributes[$name]->type == Attribute::TYPE_STRING) {
                $filters[] = ['like', $name, $params[$name]];
            } else {
                $filters[] = ['=', $name, $params[$name]];
            }
        }
        if (empty($filters) and $this->defaultFilters) {
            $filters = $this->defaultFilters;
        }
        return $filters;
    }

    /**
     * @param DataProvider $provider
     */
    public function meta($provider)
    {
        $provider->output->addMeta('filter', $this->attributes);
    }
}