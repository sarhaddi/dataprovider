<?php

namespace app\services\dataProvider\base;

use yii\helpers\ArrayHelper;

/**
 * Class Sort
 * @package app\services\dataProvider\base
 */
class Sort extends Unit
{
    public $attributes = [];
    public $defaultOrder;

    public function getOrders()
    {
        $attributeOrders = [];
        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();
        if (isset($params['sort'])) {
            foreach ($this->parseSortParam($params['sort']) as $attribute) {
                $descending = false;
                if (strncmp($attribute, '-', 1) === 0) {
                    $descending = true;
                    $attribute = substr($attribute, 1);
                }

                if (in_array($attribute, $this->attributes)) {
                    $attributeOrders[$attribute] = $descending ? SORT_DESC : SORT_ASC;
                }
            }
        }
        if (empty($attributeOrders) and $this->defaultOrder) {
            $attributeOrders = $this->defaultOrder;
        }
        return $attributeOrders;
    }

    protected function parseSortParam($param)
    {
        return is_scalar($param) ? explode(',', $param) : [];
    }

    /**
     * @param DataProvider $provider
     */
    public function meta($provider)
    {
        $provider->output->addMeta('sort', $this->attributes);
    }
}