<?php

namespace app\services\dataProvider\base;

use app\services\dataProvider\base\DataProvider;
use yii\web\Link;

/**
 * Class Pagination
 * @property-read int $limit The limit of the data.
 * @property-read int $offset The offset of the data.
 * @property-read int $pageCount Number of pages.
 * @property int $pageSize The number of items per page.
 * @package app\services\dataProvider\base
 */
class Pagination extends Unit
{
    /**
     * @var string name of the parameter storing the current page index.
     * @see params
     */
    public $pageParam = 'page';
    /**
     * @var string name of the parameter storing the page size.
     * @see params
     */
    public $pageSizeParam = 'per-page';
    /**
     * @var int total number of items.
     */
    public $totalCount = 0;
    /**
     * @var int the default page size. This property will be returned by [[pageSize]] when page size
     * cannot be determined by [[pageSizeParam]] from [[params]].
     */
    public $defaultPageSize = 20;
    /**
     * @var array|false the page size limits. The first array element stands for the minimal page size, and the second
     * the maximal page size. If this is false, it means [[pageSize]] should always return the value of [[defaultPageSize]].
     */
    public $pageSizeLimit = [1, 50];

    protected $_pageSize;

    protected $_page;

    /**
     * @return int number of pages
     */
    public function getPageCount()
    {
        $pageSize = $this->getPageSize();
        if ($pageSize < 1) {
            return $this->totalCount > 0 ? 1 : 0;
        }

        $totalCount = $this->totalCount < 0 ? 0 : $this->totalCount;

        return (int)(($totalCount + $pageSize - 1) / $pageSize);
    }

    /**
     * @return int current page number.
     */
    public function getPage()
    {
        if ($this->_page === null) {
            $page = (int)$this->getQueryParam($this->pageParam, 1) - 1;
            $this->setPage($page);
        }

        return $this->_page;
    }

    /**
     * Sets the current page number.
     * @param int $value index of the current page.
     */
    public function setPage($value)
    {
        if ($value === null) {
            $this->_page = null;
        } else {
            $value = (int)$value;
            $pageCount = $this->getPageCount();
            if ($value >= $pageCount) {
                $value = $pageCount - 1;
            }
            if ($value < 0) {
                $value = 0;
            }
            $this->_page = $value;
        }
    }

    /**
     * @return int the number of items per page.
     */
    public function getPageSize()
    {
        if ($this->_pageSize === null) {
            if (empty($this->pageSizeLimit) || !isset($this->pageSizeLimit[0], $this->pageSizeLimit[1])) {
                $pageSize = $this->defaultPageSize;
                $this->setPageSize($pageSize);
            } else {
                $pageSize = (int)$this->getQueryParam($this->pageSizeParam, $this->defaultPageSize);
                $this->setPageSize($pageSize);
            }
        }

        return $this->_pageSize;
    }

    /**
     * @param int $value the number of items per page.
     */
    public function setPageSize($value)
    {
        if ($value === null) {
            $this->_pageSize = null;
        } else {
            $value = (int)$value;
            if (isset($this->pageSizeLimit[0], $this->pageSizeLimit[1])) {
                if ($value < $this->pageSizeLimit[0]) {
                    $value = $this->pageSizeLimit[0];
                } elseif ($value > $this->pageSizeLimit[1]) {
                    $value = $this->pageSizeLimit[1];
                }
            }
            $this->_pageSize = $value;
        }
    }

    /**
     * @return int the offset of the data.
     */
    public function getOffset()
    {
        $pageSize = $this->getPageSize();

        return $pageSize < 1 ? 0 : $this->getPage() * $pageSize;
    }

    /**
     * @return int the limit of the data.
     */
    public function getLimit()
    {
        $pageSize = $this->getPageSize();

        return $pageSize < 1 ? -1 : $pageSize;
    }

    /**
     * Returns the value of the specified query parameter.
     * @param string $name the parameter name
     * @param string $defaultValue the value to be returned when the specified parameter does not exist
     * @return string|null the parameter value
     */
    protected function getQueryParam($name, $defaultValue = null)
    {
        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();

        return isset($params[$name]) && is_scalar($params[$name]) ? $params[$name] : $defaultValue;
    }

    /**
     * @param DataProvider $provider
     */
    public function meta($provider)
    {
        $this->totalCount = $this->getTotalCount($provider);
        $provider->output->addMeta('pagination', [
            'totalCount' => $this->totalCount,
            'pageCount' => $this->getPageCount(),
            'perPage' => $this->pageSize
        ]);
    }
    /**
     * @param DataProvider $provider
     */
    public function data($provider)
    {
        $this->totalCount = $this->getTotalCount($provider);
        $response = \Yii::$app->response;

        $response->getHeaders()
            ->set('X-Pagination-Total-Count', $this->totalCount)
            ->set('X-Pagination-Page-Count', $this->getPageCount())
            ->set('X-Pagination-Current-Page', $this->getPage() + 1)
            ->set('X-Pagination-Per-Page', $this->pageSize);
    }

    /**
     * @param DataProvider $provider
     */
    public function getTotalCount($provider)
    {
        return 0;
    }
}